
#include "decoder_io_context.h"
#include "encoder_io_context.h"
#include "file_io.h"
#include "aac_module.h"

static DecoderContext dec_context;
static EncoderContext enc_context;

static int decode_audio_file(DecoderContext *dec_context, const char *in_file_name, const char *out_file_name)
{
    IO io = {0};
    DecoderIOContext io_ctx = {&io};
    init_dec_io_context(&io_ctx, dec_context);
    
    if(init_file_io(&io, in_file_name, out_file_name) < 0)
        return -1;
    
    if(decode(dec_context) < 0)
        return -1;
    
    deinit_dec_io_context(&io_ctx, dec_context);
    close_file_io(&io);
    return 0;
}

static int encode_audio_file(EncoderContext *enc_context, const char *in_file_name, const char *out_file_name)
{
    IO io = {0};
    EncoderIOContext enc_io_ctx = { &io};
    init_enc_io_context(&enc_io_ctx, enc_context, out_file_name);
    
    if(init_file_io(&io, in_file_name, NULL) < 0)
        return -1;
    
    if(encode_audio(enc_context) < 0)
        return -1;
    
    deinit_enc_io_context(&enc_io_ctx, enc_context);
    close_file_io(&io);
    return 0;
}

void encode(const char *in_file, const char *out_file)
{
    const char *tmp_file = "./tmp.pcm";
    decode_audio_file(&dec_context, in_file, tmp_file);
    encode_audio_file(&enc_context, tmp_file, out_file);
}

int init()
{
    AVDictionary *opt = NULL;
    SampleDesc dec_sample_desc = {8000, AV_SAMPLE_FMT_S16, AV_CH_LAYOUT_MONO, AV_CODEC_ID_PCM_MULAW};
    SampleDesc enc_sample_desc = {8000, AV_SAMPLE_FMT_FLTP, AV_CH_LAYOUT_MONO, AV_CODEC_ID_AAC};
    av_register_all();
    init_decoder(&dec_context, &dec_sample_desc);
    init_encoder(&enc_context, &enc_sample_desc, opt);
    return 0;
}

int deinit()
{
    deinit_decoder(&dec_context);
    deinit_encoder(&enc_context);
    return 0;
}
