
#ifndef aac_module_h
#define aac_module_h

int init();
int deinit();
void encode(const char *in_file, const char *out_file);

#endif /* aac_module_h */
