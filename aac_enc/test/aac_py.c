#include <Python.h>
#include "aac_module.h"

static PyObject *encode_py(PyObject *self, PyObject *args)
{
    char *input;
    char *output;
    
    if (!PyArg_ParseTuple(args, "ss", &input, &output)) {
        return NULL;
    }
    encode(input, output);
    Py_RETURN_NONE;
}

//static PyObject *init_py(PyObject *self, PyObject *args)
//{
//    init();
//}
//
//static PyObject *deinit_py(PyObject *self, PyObject *args)
//{
//    deinit();
//}

static PyMethodDef aac_methods[] = {
    { "encode", encode_py, METH_VARARGS, "" },
//    { "init", init_py, METH_VARARGS, "" },
//    { "deinit", deinit_py, METH_VARARGS, "" },
    { NULL, NULL, 0, NULL }
};


DL_EXPORT(void) initaac(void)
{
    Py_InitModule("aac", aac_methods);
    init();
}

