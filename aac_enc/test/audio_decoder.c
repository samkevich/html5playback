
#include "audio_decoder.h"

int decode_frame(DecoderContext* context, AVFrame *decoded_frame)
{
    int len = 0;
    int got_frame = 0;
    AVPacket *avpkt = NULL;
    while (!got_frame && context->read_packet_cb(context->io_context, &avpkt)) {
        len = avcodec_decode_audio4(context->codec_context, decoded_frame, &got_frame, avpkt);
        if (len < 0) {
            fprintf(stderr, "Error while decoding\n");
            return -1;
        }
        avpkt->dts = avpkt->pts = AV_NOPTS_VALUE;
        if (avpkt->size > len) {
            /* Refill the input buffer, to avoid trying to decode
             * incomplete frames. Instead of this, one could also use
             * a parser, or use a proper container format through
             * libavformat. */
            memmove(avpkt->data, avpkt->data + len, avpkt->size - len);
        }
        avpkt->size -= len;
    }
    return got_frame;
}

int decode(DecoderContext* context)
{
    AVFrame *decoded_frame;
    if (!(decoded_frame = av_frame_alloc())) {
        fprintf(stderr, "Could not allocate audio frame\n");
        return -1;
    }
    
    while(decode_frame(context, decoded_frame))
    {
        context->write_frame_cb(context->io_context, decoded_frame);
    }
    
    av_frame_free(&decoded_frame);
    return 0;
}

int init_decoder(DecoderContext* context, SampleDesc* desc)
{
    context->codec = avcodec_find_decoder(desc->codec_id);
    if (!context->codec) {
        fprintf(stderr, "Codec not found\n");
        return -1;
    }
    context->codec_context = avcodec_alloc_context3(context->codec);
    if (!context->codec_context) {
        fprintf(stderr, "Could not allocate audio codec context\n");
        return -1;
    }
    context->codec_context->sample_rate = desc->sample_rate;
    //context->codec_context->sample_fmt = desc->sample_fmt;
    context->codec_context->channel_layout = desc->channel_layout;
    context->codec_context->channels = av_get_channel_layout_nb_channels(desc->channel_layout);
    
    if (avcodec_open2(context->codec_context, context->codec, NULL) < 0) {
        fprintf(stderr, "Could not open codec\n");
        return -1;
    }

    return 0;
}

int deinit_decoder(DecoderContext* context)
{
    avcodec_close(context->codec_context);
    av_free(context->codec_context);
    return 0;
}
