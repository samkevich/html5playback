
#ifndef audio_decoder_h
#define audio_decoder_h

#include <libavcodec/avcodec.h>
#include <stdbool.h>
#include "sample_desc.h"

typedef bool(*ReadPacketCB)(void* context, AVPacket **packet);
typedef bool(*WriteFrameCB)(void* context, AVFrame *frame);

typedef struct DecoderContext
{
    AVCodec *codec;
    AVCodecContext* codec_context;
    ReadPacketCB read_packet_cb;
    WriteFrameCB write_frame_cb;
    void* io_context;
} DecoderContext;

int init_decoder(DecoderContext* context, SampleDesc* desc);

int deinit_decoder(DecoderContext* context);

int decode_frame(DecoderContext* context, AVFrame *decoded_frame);

int decode(DecoderContext* context);

#endif /* audio_decoder_h */
