
#include "audio_encoder.h"

void open_resampler(EncoderContext* enc_context)
{
    int ret = 0;
    /* create resampler context */
    enc_context->swr_ctx = swr_alloc();
    if (!enc_context->swr_ctx) {
        fprintf(stderr, "Could not allocate resampler context\n");
        exit(1);
    }
    /* set options */
    av_opt_set_int       (enc_context->swr_ctx, "in_channel_count",   enc_context->enc->channels,       0);
    av_opt_set_int       (enc_context->swr_ctx, "in_sample_rate",     enc_context->enc->sample_rate,    0);
    av_opt_set_sample_fmt(enc_context->swr_ctx, "in_sample_fmt",      AV_SAMPLE_FMT_S16, 0);
    av_opt_set_int       (enc_context->swr_ctx, "out_channel_count",  enc_context->enc->channels,       0);
    av_opt_set_int       (enc_context->swr_ctx, "out_sample_rate",    enc_context->enc->sample_rate,    0);
    av_opt_set_sample_fmt(enc_context->swr_ctx, "out_sample_fmt",     enc_context->enc->sample_fmt,     0);
    
    /* initialize the resampling context */
    if ((ret = swr_init(enc_context->swr_ctx)) < 0) {
        fprintf(stderr, "Failed to initialize the resampling context\n");
        exit(1);
    }
}

void open_codec(EncoderContext* enc_context, SampleDesc* sample_desc, AVDictionary *opt_arg)
{
    int ret = 0;
    AVCodec *codec;
    AVCodecContext *codec_context;
    codec = avcodec_find_encoder(sample_desc->codec_id);
    if (!(codec)) {
        fprintf(stderr, "Could not find encoder for '%s'\n",
                avcodec_get_name(sample_desc->codec_id));
        exit(1);
    }
    codec_context = avcodec_alloc_context3(codec);
    if (!codec_context) {
        fprintf(stderr, "Could not alloc an encoding context\n");
        exit(1);
    }
    
    codec_context->sample_fmt  = sample_desc->sample_fmt;//codec->sample_fmts ? codec->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
    codec_context->bit_rate = 64000;
    codec_context->sample_rate = sample_desc->sample_rate;
    codec_context->channel_layout = sample_desc->channel_layout;
    codec_context->channels = av_get_channel_layout_nb_channels(codec_context->channel_layout);
    enc_context->codec = codec;
    enc_context->enc = codec_context;
    
    AVDictionary *opt = NULL;
    av_dict_copy(&opt, opt_arg, 0);
    ret = avcodec_open2(codec_context, enc_context->codec, &opt);
    av_dict_free(&opt);
    if (ret < 0) {
        fprintf(stderr, "Could not open audio codec: %s\n", av_err2str(ret));
        exit(1);
    }
}

AVFrame *alloc_audio_frame(enum AVSampleFormat sample_fmt,
                                  uint64_t channel_layout,
                                  int sample_rate,
                                  int nb_samples)
{
    AVFrame *frame = av_frame_alloc();
    int ret;
    if (!frame) {
        fprintf(stderr, "Error allocating an audio frame\n");
        exit(1);
    }
    frame->format = sample_fmt;
    frame->channel_layout = channel_layout;
    frame->sample_rate = sample_rate;
    frame->nb_samples = nb_samples;
    if (nb_samples) {
        ret = av_frame_get_buffer(frame, 0);
        if (ret < 0) {
            fprintf(stderr, "Error allocating an audio buffer\n");
            exit(1);
        }
    }
    return frame;
}

void init_encoder(EncoderContext* enc_context, SampleDesc* sample_desc, AVDictionary *opt_arg)
{
    int nb_samples;
    open_codec(enc_context, sample_desc, opt_arg);
    
    if (enc_context->enc->codec->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)
        nb_samples = 10000;
    else
        nb_samples = enc_context->enc->frame_size;
    enc_context->frame     = alloc_audio_frame(enc_context->enc->sample_fmt, enc_context->enc->channel_layout,
                                               enc_context->enc->sample_rate, nb_samples);
    open_resampler(enc_context);
}


//return 1 when has encoded packet, 0 otherwise
int encode_frame(EncoderContext* enc_context, AVPacket *pkt)
{
    AVCodecContext *c;
    AVFrame *frame = NULL;
    int ret;
    int got_packet = 0;
    int dst_nb_samples;
    c = enc_context->enc;
    //*pkt = { 0 }; // data and size must be 0;
    pkt->data = NULL;
    pkt->size = 0;
    av_init_packet(pkt);
    
    while(!got_packet || !frame)
    {
        if(enc_context->read_frame_cb(enc_context->io_context, &frame))
        {
//            frame->pts = enc_context->next_pts;
//            enc_context->next_pts  += frame->nb_samples;
        }
        
        if (frame) {
            dst_nb_samples = av_rescale_rnd(swr_get_delay(enc_context->swr_ctx, c->sample_rate) + frame->nb_samples,
                                            c->sample_rate, c->sample_rate, AV_ROUND_UP);
            av_assert0(dst_nb_samples == frame->nb_samples);
            /* when we pass a frame to the encoder, it may keep a reference to it
             * internally;
             * make sure we do not overwrite it here
             */
            ret = av_frame_make_writable(enc_context->frame);
            if (ret < 0)
                exit(1);
            /* convert to destination format */
            ret = swr_convert(enc_context->swr_ctx,
                              enc_context->frame->data, dst_nb_samples,
                              (const uint8_t **)frame->data, frame->nb_samples);
            if (ret < 0) {
                fprintf(stderr, "Error while converting\n");
                exit(1);
            }
            frame = enc_context->frame;
            frame->pts = av_rescale_q(enc_context->samples_count, (AVRational){1, c->sample_rate}, c->time_base);
            enc_context->samples_count += dst_nb_samples;
        
            ret = avcodec_encode_audio2(c, pkt, frame, &got_packet);
            if (ret < 0) {
                fprintf(stderr, "Error encoding audio frame: %s\n", av_err2str(ret));
                exit(1);
            }
        }
        
        if(!frame && !got_packet)
            break;
    }
    
    return got_packet;
}

int encode_audio(EncoderContext* enc_context)
{
    AVPacket pkt;
    while (encode_frame(enc_context, &pkt)) {
        if(!enc_context->write_packet_cb(enc_context->io_context, &pkt))
            return -1;
    }
    return 0;
}

void deinit_encoder(EncoderContext* enc_context)
{
    avcodec_free_context(&enc_context->enc);
    av_frame_free(&enc_context->frame);
    swr_free(&enc_context->swr_ctx);
}

