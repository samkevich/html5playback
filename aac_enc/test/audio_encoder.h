
#ifndef audio_encoder_h
#define audio_encoder_h

#include <libavutil/avassert.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
#include <stdbool.h>
#include "sample_desc.h"

typedef bool(*ReadFrameCB)(void* context, AVFrame **frame);
typedef bool(*WritePacketCB)(void* context, AVPacket *packet);

typedef struct EncoderContext
{
    AVCodec *codec;
    AVCodecContext *enc;
    /* pts of the next frame that will be generated */
    int64_t next_pts;
    int samples_count;
    AVFrame *frame;
    struct SwrContext *swr_ctx;
    ReadFrameCB read_frame_cb;
    WritePacketCB write_packet_cb;
    void* io_context;
} EncoderContext;

void init_encoder(EncoderContext* enc_context, SampleDesc* sample_desc, AVDictionary *opt_arg);
int encode_frame(EncoderContext* enc_context, AVPacket *pkt);
int encode_audio(EncoderContext* enc_context);
void deinit_encoder(EncoderContext* enc_context);

AVFrame *alloc_audio_frame(enum AVSampleFormat sample_fmt,
                           uint64_t channel_layout,
                           int sample_rate,
                           int nb_samples);

#endif /* audio_encoder_h */
