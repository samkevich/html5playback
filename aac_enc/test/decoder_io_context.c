
#include "decoder_io_context.h"

bool read_packet_cb(void* context, AVPacket **packet)
{
    DecoderIOContext* io_ctx = (DecoderIOContext*)context;
    int len = (int)io_ctx->io->read_cb(io_ctx->io->context,
                                       io_ctx->avpkt.data + io_ctx->avpkt.size,
                                       AUDIO_INBUF_SIZE - io_ctx->avpkt.size,
                                       0,
                                       CUR);
    
    if(len > 0)
    {
        io_ctx->avpkt.size += len;
        *packet = &io_ctx->avpkt;
        return true;
    }
    packet = NULL;
    return false;
}

bool write_frame_cb(void* context, AVFrame *frame)
{
    DecoderIOContext* io_ctx = (DecoderIOContext*)context;
    int data_size = av_get_bytes_per_sample(io_ctx->codec_context->sample_fmt);
    if (data_size < 0) {
        /* This should not occur, checking just for paranoia */
        fprintf(stderr, "Failed to calculate data size\n");
        return false;
    }
    for (int i=0; i< frame->nb_samples; i++)
        for (int ch=0; ch<io_ctx->codec_context->channels; ch++)
            if(io_ctx->io->write_cb(io_ctx->io->context, frame->data[ch] + data_size*i, data_size, 0, CUR) != data_size)
                return false;
    return true;
}

void init_dec_io_context(DecoderIOContext *io_ctx, DecoderContext *dec_ctx)
{
    io_ctx->buf = (uint8_t*)malloc(AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE);
    av_init_packet(&io_ctx->avpkt);
    io_ctx->avpkt.data = io_ctx->buf;
    io_ctx->avpkt.size = 0;
    io_ctx->codec_context = dec_ctx->codec_context;
    dec_ctx->read_packet_cb = read_packet_cb;
    dec_ctx->write_frame_cb = write_frame_cb;
    dec_ctx->io_context = io_ctx;
}

void deinit_dec_io_context(DecoderIOContext *io_ctx, DecoderContext *dec_ctx)
{
    free(io_ctx->buf);
    dec_ctx->read_packet_cb = NULL;
    dec_ctx->write_frame_cb = NULL;
    dec_ctx->io_context = NULL;
}
