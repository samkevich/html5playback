
#ifndef decoder_io_context_h
#define decoder_io_context_h

#include "audio_decoder.h"
#include "io.h"

#define AUDIO_INBUF_SIZE 2048

typedef struct DecoderIOContext
{
    IO* io;
    AVCodecContext* codec_context;
    AVPacket avpkt;
    uint8_t* buf;
} DecoderIOContext;

bool read_packet_cb(void* context, AVPacket **packet);

bool write_frame_cb(void* context, AVFrame *frame);

void init_dec_io_context(DecoderIOContext *io_ctx, DecoderContext *dec_ctx);

void deinit_dec_io_context(DecoderIOContext *io_ctx, DecoderContext *dec_ctx);


#endif /* decoder_io_context_h */
