
#include "encoder_io_context.h"
#include <libavutil/timestamp.h>

/* Add an output stream. */
void add_stream(OutputStream *ost, EncoderContext* enc_context, AVFormatContext *oc)
{
    ost->stream = avformat_new_stream(oc, NULL);
    if (!ost->stream) {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }
    ost->stream->id = oc->nb_streams-1;
    ost->stream->time_base = (AVRational){ 1, enc_context->enc->sample_rate };
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        enc_context->enc->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    
    /* copy the stream parameters to the muxer */
    int ret = avcodec_parameters_from_context(ost->stream->codecpar, enc_context->enc);
    if (ret < 0) {
        fprintf(stderr, "Could not copy the stream parameters\n");
        exit(1);
    }
}

void open_out_stream(OutputStream *audio_st, const char* filename)
{
    int ret;
    /* allocate the output media context */
    avformat_alloc_output_context2(&audio_st->format_context, NULL, NULL, filename);
    if (!audio_st->format_context) {
        printf("Could not deduce output format from file extension: using MPEG.\n");
        exit(1);
    }
    if (!audio_st->format_context)
        exit(1);
    
    av_dump_format(audio_st->format_context, 0, filename, 1);
    /* open the output file, if needed */
    if (!(audio_st->format_context->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&audio_st->format_context->pb, filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open '%s': %s\n", filename,
                    av_err2str(ret));
            exit(1);
        }
    }
}

void close_out_stream(OutputStream *audio_st)
{
    if (!(audio_st->format_context->oformat->flags & AVFMT_NOFILE))
    /* Close the output file. */
        avio_closep(&audio_st->format_context->pb);
    /* free the stream */
    avformat_free_context(audio_st->format_context);
}

void write_header(OutputStream *audio_st, AVDictionary **options)
{
    int ret;
    /* Write the stream header, if any. */
    ret = avformat_write_header(audio_st->format_context, options);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file: %s\n",
                av_err2str(ret));
        exit(1);
    }
}

void write_trailer(OutputStream *audio_st)
{
    /* Write the trailer, if any. The trailer must be written before you
     * close the CodecContexts open when you wrote the header; otherwise
     * av_write_trailer() may try to use memory that was freed on
     * av_codec_close(). */
    av_write_trailer(audio_st->format_context);
}

bool read_frame(void* context, AVFrame **frame)
{
    EncoderIOContext* io_ctx = (EncoderIOContext*)context;
    int readed = 0;
    readed = io_ctx->io->read_cb(io_ctx->io->context,
                                 io_ctx->frame->data[0],
                                 io_ctx->frame_size,
                                 0,
                                 CUR);
    if(readed != io_ctx->frame_size)
    {
        *frame = NULL;
        return false;
    }
    *frame = io_ctx->frame;
    return true;
}

static void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;
    printf("pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
           av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
           av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
           av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
           pkt->stream_index);
}

bool write_packet(void* context, AVPacket *packet)
{
    EncoderIOContext* io_ctx = (EncoderIOContext*)context;
    /* rescale output packet timestamp values from codec to stream timebase */
    //av_packet_rescale_ts(packet, *time_base, io_ctx->out_stream->stream->time_base);
    packet->stream_index = io_ctx->out_stream->stream->index;
    /* Write the compressed frame to the media file. */
    log_packet(io_ctx->out_stream->format_context, packet);
    return !av_interleaved_write_frame(io_ctx->out_stream->format_context, packet);
}

void init_enc_io_context(EncoderIOContext *io_ctx, EncoderContext *enc_ctx, const char* out_file_name)
{
    AVDictionary *opt = NULL;
    OutputStream *audio_st = (OutputStream *)malloc(sizeof(OutputStream));
    open_out_stream(audio_st, out_file_name);
    add_stream(audio_st, enc_ctx, audio_st->format_context);
    write_header(audio_st, &opt);
    io_ctx->out_stream = audio_st;
    io_ctx->codec_time_base = &enc_ctx->enc->time_base;
    io_ctx->frame = alloc_audio_frame(AV_SAMPLE_FMT_S16,
                                      enc_ctx->frame->channel_layout,
                                      enc_ctx->frame->sample_rate,
                                      enc_ctx->frame->nb_samples);
    io_ctx->frame_size = enc_ctx->frame->nb_samples * enc_ctx->enc->channels * av_get_bytes_per_sample((enum AVSampleFormat)io_ctx->frame->format);
    enc_ctx->read_frame_cb = read_frame;
    enc_ctx->write_packet_cb = write_packet;
    enc_ctx->io_context = io_ctx;
}

void deinit_enc_io_context(EncoderIOContext *io_ctx, EncoderContext *enc_ctx)
{
    write_trailer(io_ctx->out_stream);
    av_frame_free(&io_ctx->frame);
    close_out_stream(io_ctx->out_stream);
    free(io_ctx->out_stream);
    enc_ctx->read_frame_cb = NULL;
    enc_ctx->write_packet_cb = NULL;
    enc_ctx->io_context = NULL;
}

