
#ifndef encoder_io_context_h
#define encoder_io_context_h

#include "audio_encoder.h"
#include "io.h"

// a wrapper around a single output AVStream
typedef struct OutputStream {
    AVStream *stream;
    AVFormatContext *format_context;
} OutputStream;

typedef struct EncoderIOContext
{
    IO* io;
    OutputStream * out_stream;
    AVRational *codec_time_base;
    AVFrame *frame;
    int frame_size;
} EncoderIOContext;

void init_enc_io_context(EncoderIOContext *io_ctx, EncoderContext *enc_ctx, const char* out_file_name);
void deinit_enc_io_context(EncoderIOContext *io_ctx, EncoderContext *enc_ctx);

#endif /* encoder_io_context_h */
