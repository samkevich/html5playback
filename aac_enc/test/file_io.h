
#ifndef file_io_h
#define file_io_h

#include "io.h"

typedef struct FileIOContext
{
    FILE* in_file;
    FILE* out_file;
} FileIOContext;

size_t file_read(void* context, unsigned char* buffer, size_t size, size_t offset, enum SeekOrigin whence)
{
    FileIOContext* io_context = (FileIOContext*)context;
    fseek(io_context->in_file, offset, whence);
    return fread(buffer, sizeof(unsigned char), size, io_context->in_file);
}

size_t file_write(void* context, const unsigned char * buffer, size_t size, size_t offset, enum SeekOrigin whence)
{
    FileIOContext* io_context = (FileIOContext*)context;
    fseek(io_context->out_file, offset, whence);
    return fwrite(buffer, sizeof(unsigned char), size, io_context->out_file);
}

int init_file_io(IO* io, const char* in_file_name, const char * out_file_name)
{
    FILE* in_file = fopen(in_file_name, "rb");
    if (!in_file && in_file_name) {
        fprintf(stderr, "Could not open %s\n", in_file_name);
        return -1;
    }
    
    FILE* out_file = fopen(out_file_name, "wb");
    if (!out_file && out_file_name) {
        fprintf(stderr, "Could not open %s\n", out_file_name);
        return -1;
    }
    
    FileIOContext* io_context = (FileIOContext*)malloc(sizeof(FileIOContext));
    io_context->in_file = in_file;
    io_context->out_file = out_file;
    io->context = io_context;
    io->read_cb = file_read;
    io->write_cb = file_write;
    return 0;
}

int close_file_io(IO* io)
{
    FileIOContext* io_context = (FileIOContext*)io->context;
    fclose(io_context->in_file);
    fclose(io_context->out_file);
    free(io_context);
    return 0;
}


#endif /* file_io_h */
