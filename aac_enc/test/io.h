
#ifndef io_h
#define io_h

enum SeekOrigin {
    BEGIN,
    CUR,
    END
};

typedef size_t(*ReadCB)(void* context, unsigned char* buffer, size_t size, size_t offset, enum SeekOrigin whence);
typedef size_t(*WriteCB)(void* context, const unsigned char * buffer, size_t size, size_t offset, enum SeekOrigin whence);

typedef struct IO {
    ReadCB read_cb;
    WriteCB write_cb;
    void* context;
} IO;

#endif /* io_h */
