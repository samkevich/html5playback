
#ifndef sample_desc_h
#define sample_desc_h

typedef struct SampleDesc
{
    int sample_rate;
    enum AVSampleFormat sample_fmt;
    uint64_t channel_layout;
    enum AVCodecID codec_id;
} SampleDesc;

#endif /* sample_desc_h */
