from distutils.core import setup, Extension

extension_mod = Extension("aac", 
    include_dirs=['/usr/local/include', './'],
    libraries=["avcodec", "avutil", "avformat", "swresample", "swscale"],
    library_dirs=['/usr/local/lib'],
    sources = ['aac_py.c', 'aac_module.c', 'audio_encoder.c', 'encoder_io_context.c', 'decoder_io_context.c', 'audio_decoder.c']
)

setup(name = "aac", ext_modules=[extension_mod])
