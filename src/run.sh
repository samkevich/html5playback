#!/usr/bin/env bash

#URL='rtsp://testuser:1234567@192.168.1.226/h264'
#URL='rtsp://testuser:1234567@192.168.2.136:554/cam/realmonitor?channel=1&subtype=1'
#URL='rtsp://testuser:synesis2014@192.168.2.139:554/onvif/profile2/media.smp'
URL='rtsp://testuser:1234567@172.20.9.112:554/Streaming/Channels/1?transportmode=unicast&profile=Profile_1'

trap ctrl_c INT

function ctrl_c() {
    kill $HTTP_PID
    kill $SOCKET_PID
}
`ipconfig getifaddr en0 > my_ip` 
python -m SimpleHTTPServer 8888 & HTTP_PID=$!
echo 'HTTP_PID='$HTTP_PID
python ./server.py $URL & SOCKET_PID=$!
echo 'SOCKET_PID='$SOCKET_PID
open http://localhost:8888/index.html

wait $SOCKET_PID
wait $HTTP_PID
