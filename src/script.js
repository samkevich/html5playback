var video = document.querySelector('video');
var mediaSource = null;
var sourceBuffer = null;
var mimeCodec = 'video/mp4; codecs="avc1.4D0029, mp4a.40.2"';
//var mimeCodec = 'video/mp4; codecs="avc1.4D0029"';
var time = 0;
var last_playback_time = 0;

var signalingChannel;

var client = new XMLHttpRequest();
client.open('GET', 'my_ip');
client.onreadystatechange = function()
{
	if(client.readyState === 4 && client.status === 200)
	{
		var addr = "ws://"+client.responseText.trim()+":8800/";
		console.log("Connect to", addr)
		signalingChannel = new WebSocket(addr);
		signalingChannel.binaryType = "arraybuffer";
		signalingChannel.addEventListener("open", connectionOpen);	
		signalingChannel.onmessage = function (evt) 
		{
			onSegmentFetched(evt.data);
		}	
	}
}
client.send();

function resize()
{
	video.width = window.innerWidth;
	video.height = window.innerHeight;
}

window.addEventListener('resize', resize);

function connectionOpen(_)
{
	if ('MediaSource' in window && MediaSource.isTypeSupported(mimeCodec)) 
	{
		mediaSource = new MediaSource;
		video.src = URL.createObjectURL(mediaSource);
		mediaSource.addEventListener('sourceopen', sourceOpen);
		mediaSource.addEventListener('sourceended', function(e) { console.log('sourceended: ' + mediaSource.readyState); });
		mediaSource.addEventListener('sourceclose', function(e) { console.log('sourceclose: ' + mediaSource.readyState); });
		mediaSource.addEventListener('error', function(e) { console.log('error: ' + mediaSource.readyState + checkSourceError(video)); });
	} 
	else 
	{
		console.error('Unsupported MIME type or codec: ', mimeCodec);
	}
}

function sourceOpen (_) 
{
    console.log('sourceopen: ' + mediaSource.readyState);
    sourceBuffer = mediaSource.addSourceBuffer(mimeCodec);
    sourceBuffer.addEventListener('error', function(e) 
    {
    	 console.log('sourceBuffer error: ', e); 
    });
    sourceBuffer.addEventListener('abort', function(e) { console.log('sourceBuffer abort: '); });
    resize();
	fetchSegment(0);
};

function fetchSegment (num) 
{
    signalingChannel.send('getSegment ' + num);
};

var t =0;
function onSegmentFetched (chunk) 
{
    data = new Uint8Array(chunk)
    if (!mediaSource.sourceBuffers[0].updating && mediaSource.readyState == "open" )
    {
    	var appendTime = sourceBuffer.buffered.length > 0 ? sourceBuffer.buffered.end(0) : 0;
    	latency =  appendTime - video.currentTime;
    	console.log("clinet latency", latency, "bufTime", appendTime, "videoTime", video.currentTime);
    	if(latency > 3 && !video.paused)
    	{
    		video.currentTime = appendTime;
    		video.play(); 		
    	}
   		
		mediaSource.sourceBuffers[0].timestampOffset = appendTime;
		//t+= 0.384;
        mediaSource.sourceBuffers[0].updating = true;
        mediaSource.sourceBuffers[0].appendBuffer(data);
        mediaSource.sourceBuffers[0].updating = false;       
    }
};
