#!/usr/bin/python -O

import sys
import time
import struct
import threading
import subprocess
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
import live555
import aac

cur_time = lambda: int(round(time.time() * 1000))
lock = threading.Lock()
AAC_FRAME_SIZE = 1024

NALU_TYPES = {
    1: "non-IDR",
    5: "IDR",
    7: "SPS",   #Sequence parameter set,
    8: "PPS"    #Picture parameter set
}

def execute(cmd):
    try:
        subprocess.check_call(cmd, shell = True)
    except Exception as err:
        print err

def readFile(name):
    with open(name, "rb") as f:
        return bytearray(f.read())

def writeFile(name, data, access = 'wb'):
    with open(name, access) as f:
        f.write(data)

def executeFrag(desc):
    pts_delta = desc.get_pts_delta()
    RAW_H264 = '../testset/frame.h264'
    FRAG_MP4 = '../testset/frag.mp4'
    UTIL_PATH = "../utils/Debug/"
    MP4FRAG = UTIL_PATH + "mp4fragment {options} {i} {o}"
    audio_len = len(desc.audioBuf) / AAC_FRAME_SIZE * AAC_FRAME_SIZE
    if audio_len > 0:
        audio = desc.audioBuf[:audio_len]
        executeAudioEncode(audio)
        desc.audioBuf = desc.audioBuf[audio_len:]
    writeFile(RAW_H264, desc.videoBuf)
    desc.videoBuf = b''
    options = ''
    options += ' --pts ' + str(pts_delta) if pts_delta > 0 else ''
    options += ' --fragment_only' if desc.fragment_only else ''
    options += ' --audio ../testset/audio.adts' if audio_len > 0 else ''
    execute(MP4FRAG.format(i = RAW_H264, o = FRAG_MP4, options = options))    
    return readFile(FRAG_MP4)

def isKeyFrame(data):
    nalu_type = struct.unpack('!B', data[0])[0] & 0x1f
    return nalu_type == 7 

def naluTypeName(data):
    nalu_type = struct.unpack('!B', data[0])[0] & 0x1f
    return NALU_TYPES[nalu_type]

def packAndSend(connection, desc):
    if not desc.connected or len(desc.videoBuf) == 0:
       return
    data = executeFrag(desc)
    try:
        connection.sendMessage(data)
        desc.fragment_only = True
    except Exception, e:
        print "Exception wile sending message: ", e
    print 'Sended', len(data)/1024, 'kb to', connection.address, desc.get_pts_delta()

def processVideo(codec_name, data, time):
    if isKeyFrame(data):
        with lock:
            for key, value in connectors.iteritems():
                packAndSend(key, value)
                value.skip_non_key = False
                value.set_pts(time)
    with lock:
        for key, value in connectors.iteritems():
            if not value.skip_non_key:
                value.videoBuf += b'\0\0\0\1' + data

    print('Received %s NAL unit for %s: %d bytes %d ms' % (naluTypeName(data), codec_name, len(data), time))

def executeAudioEncode(data):
    print 'Audio length', len(data) 
    IN_FILE = '../testset/audio.raw'
    OUT_FILE = '../testset/audio.adts'
    writeFile(IN_FILE, data)
    aac.encode(IN_FILE, OUT_FILE)

def processAudio(codec_name, data, time):
    print codec_name, time
    with lock:
        for key, value in connectors.iteritems():
            value.audioBuf += data

def oneFrame(codecName, data, sec, usec, durUSec):
    #frame_time = usec + sec * 1000000
    frame_time = cur_time() * 1000
    if codecName == 'H264':
        processVideo(codecName, data, frame_time)
    else:
        processAudio(codecName, data, frame_time)
 
class Signaling(WebSocket):
    
    def handleMessage(self):
        with lock:
            connectors[self].connected = True
        print "handleMessage"

    def handleConnected(self):
        print self.address, 'connected'
        with lock:
            connectors[self] = ConnectorDesc()
    
    def handleClose(self):
        print self.address, 'closed'
        with lock:
            connectors.pop(self, None)

class ConnectorDesc:

    def __init__(self):
        self.skip_non_key = True
        self.connected = False 
        self.fragment_only = False
        self.videoBuf = b''
        self.audioBuf = b''
        self.time = None
        self.last_time = None
    
    def set_pts(self, time):
        self.last_time = self.time
        self.time = time

    def get_pts_delta(self):
        if self.last_time is None or self.last_time < 0:
            return 0
        return self.time - self.last_time

if __name__ == '__main__':
    rtsp_url = sys.argv[1]
    connectors = {}
    server = SimpleWebSocketServer('', 8800, Signaling)
    live555.startRTSP(rtsp_url, oneFrame, False)
    # Run Live555's event loop in a background thread:
    t = threading.Thread(target=live555.runEventLoop, args=())
    t.setDaemon(True)
    t.start()
    server.serveforever()
    live555.stopEventLoop()
    t.join()
    
