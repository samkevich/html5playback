/*****************************************************************
|
|    AP4 - MP4 Fragmenter
|
|    Copyright 2002-2015 Axiomatic Systems, LLC
|
|
|    This file is part of Bento4/AP4 (MP4 Atom Processing Library).
|
|    Unless you have obtained Bento4 under a difference license,
|    this version of Bento4 is Bento4|GPL.
|    Bento4|GPL is free software; you can redistribute it and/or modify
|    it under the terms of the GNU General Public License as published by
|    the Free Software Foundation; either version 2, or (at your option)
|    any later version.
|
|    Bento4|GPL is distributed in the hope that it will be useful,
|    but WITHOUT ANY WARRANTY; without even the implied warranty of
|    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
|    GNU General Public License for more details.
|
|    You should have received a copy of the GNU General Public License
|    along with Bento4|GPL; see the file COPYING.  If not, write to the
|    Free Software Foundation, 59 Temple Place - Suite 330, Boston, MA
|    02111-1307, USA.
|
 ****************************************************************/

/*----------------------------------------------------------------------
|   includes
+---------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "Ap4.h"

/*----------------------------------------------------------------------
|   constants
+---------------------------------------------------------------------*/
#define BANNER "MP4 Fragmenter - Version 1.6.0\n"\
               "(Bento4 Version " AP4_VERSION_STRING ")\n"\
               "(c) 2002-2015 Axiomatic Systems, LLC"

/*----------------------------------------------------------------------
|   constants
+---------------------------------------------------------------------*/

const unsigned int AP4_FRAGMENTER_OUTPUT_MOVIE_TIMESCALE      = 1000;
const unsigned int AP4_MUX_DEFAULT_VIDEO_FRAME_RATE = 24;

typedef enum {
    AP4_FRAGMENTER_FORCE_SYNC_MODE_NONE,
    AP4_FRAGMENTER_FORCE_SYNC_MODE_AUTO,
    AP4_FRAGMENTER_FORCE_SYNC_MODE_ALL
} ForceSyncMode;

/*----------------------------------------------------------------------
|   options
+---------------------------------------------------------------------*/
struct _Options {
    unsigned int  verbosity;
    bool          debug;
    bool          fragment_only;
    double        tfdt_start;
    unsigned int  sequence_number_start;
    unsigned int  pts;
    const char*   audio_file;
    ForceSyncMode force_i_frame_sync;
} Options;

/*----------------------------------------------------------------------
|   PrintUsageAndExit
+---------------------------------------------------------------------*/
static void
PrintUsageAndExit()
{
    fprintf(stderr, 
            BANNER 
            "\n\nusage: mp4fragment <input> <output>\n");
    exit(1);
}

/*----------------------------------------------------------------------
|   platform adaptation
+---------------------------------------------------------------------*/
#if defined(_MSC_VER)
static double 
strtof(char* s, char** /* end */)
{
	_CRT_DOUBLE value = {0.0};
    int result = _atodbl(&value, s);
	return result == 0 ? (double)value.x : 0.0;
}
#endif

/*----------------------------------------------------------------------
|   FragmentInfo
+---------------------------------------------------------------------*/
class FragmentInfo {
public:
    FragmentInfo(AP4_SyntheticSampleTable* sample_table, AP4_UI64 timestamp, AP4_ContainerAtom* moof) :
        m_sample_table(sample_table),
        m_Timestamp(timestamp),
        m_Duration(0),
        m_Moof(moof),
        m_MoofPosition(0),
        m_MdatSize(0) {}

    AP4_SyntheticSampleTable* m_sample_table;
    AP4_UI64            m_Timestamp;
    AP4_UI32            m_Duration;
    AP4_ContainerAtom*  m_Moof;
    AP4_Position        m_MoofPosition;
    AP4_UI32            m_MdatSize;
};

/*----------------------------------------------------------------------
 |   AddAacTrack
 +---------------------------------------------------------------------*/
static void
ParseAacTrack(AP4_ByteStream& input_stream,
            AP4_SyntheticSampleTable* sample_table, AP4_Track** track, AP4_UI32 track_id)
{
    AP4_Result result;
    // create an ADTS parser
    AP4_AdtsParser parser;
    bool           initialized = false;
    unsigned int   sample_description_index = 0;
    
    // read from the input, feed, and get AAC frames
    AP4_UI32     sample_rate = 0;
    AP4_Cardinal sample_count = 0;
    bool eos = false;
    for(;;) {
        // try to get a frame
        AP4_AacFrame frame;
        result = parser.FindFrame(frame);
        if (AP4_SUCCEEDED(result)) {
            if (Options.verbosity) {
                printf("AAC frame [%06d]: size = %d, %d kHz, %d ch\n",
                       sample_count,
                       frame.m_Info.m_FrameLength,
                       (int)frame.m_Info.m_SamplingFrequency,
                       frame.m_Info.m_ChannelConfiguration);
            }
            if (!initialized) {
                initialized = true;
                
                // create a sample description for our samples
                AP4_DataBuffer dsi;
                unsigned char aac_dsi[2];
                
                unsigned int object_type = 2; // AAC LC by default
                aac_dsi[0] = (object_type<<3) | (frame.m_Info.m_SamplingFrequencyIndex>>1);
                aac_dsi[1] = ((frame.m_Info.m_SamplingFrequencyIndex&1)<<7) | (frame.m_Info.m_ChannelConfiguration<<3);
                
                dsi.SetData(aac_dsi, 2);
                AP4_MpegAudioSampleDescription* sample_description =
                new AP4_MpegAudioSampleDescription(
                                                   AP4_OTI_MPEG4_AUDIO,   // object type
                                                   (AP4_UI32)frame.m_Info.m_SamplingFrequency,
                                                   16,                    // sample size
                                                   frame.m_Info.m_ChannelConfiguration,
                                                   &dsi,                  // decoder info
                                                   6144,                  // buffer size
                                                   128000,                // max bitrate
                                                   32000);                // average bitrate
                sample_description_index = sample_table->GetSampleDescriptionCount();
                sample_table->AddSampleDescription(sample_description);
                sample_rate = (AP4_UI32)frame.m_Info.m_SamplingFrequency;
            }
            
            AP4_ByteStream* stream = new AP4_MemoryByteStream();
            // read and store the sample data
            AP4_DataBuffer sample_data(frame.m_Info.m_FrameLength);
            sample_data.SetDataSize(frame.m_Info.m_FrameLength);
            frame.m_Source->ReadBytes(sample_data.UseData(), frame.m_Info.m_FrameLength);
            stream->Write(sample_data.GetData(), frame.m_Info.m_FrameLength);
            
            // add the sample to the table
            sample_table->AddSample(*stream, 0, frame.m_Info.m_FrameLength, 1024, sample_description_index, 0, 0, true);
            sample_count++;
            stream->Release();
        } else {
            if (eos) break;
        }
        
        // read some data and feed the parser
        AP4_UI08 input_buffer[4096];
        AP4_Size to_read = parser.GetBytesFree();
        if (to_read) {
            AP4_Size bytes_read = 0;
            if (to_read > sizeof(input_buffer)) to_read = sizeof(input_buffer);
            result = input_stream.ReadPartial(input_buffer, to_read, bytes_read);
            if (AP4_SUCCEEDED(result)) {
                AP4_Size to_feed = bytes_read;
                result = parser.Feed(input_buffer, &to_feed);
                if (AP4_FAILED(result)) {
                    AP4_Debug("ERROR: parser.Feed() failed (%d)\n", result);
                    return;
                }
            } else {
                if (result == AP4_ERROR_EOS) {
                    eos = true;
                    parser.Feed(NULL, NULL, AP4_BITSTREAM_FLAG_EOS);
                }
            }
        }
    }
    // create a sample table (with no samples) to hold the sample description
    AP4_SyntheticSampleTable* empty_sample_table = new AP4_SyntheticSampleTable();
    empty_sample_table->AddSampleDescription(sample_table->GetSampleDescription(0), false);

    // create an audio track
    *track = new AP4_Track(AP4_Track::TYPE_AUDIO,
                                     empty_sample_table,
                                     track_id,          // track id
                                     AP4_FRAGMENTER_OUTPUT_MOVIE_TIMESCALE,       // movie time scale
                                     0, //sample_count*1024, // track duration
                                     sample_rate,       // media time scale
                                     0, //sample_count*1024, // media duration
                                     "und",             // language
                                     0, 0);             // width, height
    
    
}



static void
ParseH264Stream(AP4_ByteStream& input_stream,
                AP4_AvcFrameParser& parser,
                AP4_SyntheticSampleTable* sample_table)
{
    AP4_Result result;
    // parse the input
    for (;;) {
        bool eos;
        unsigned char input_buffer[4096];
        AP4_Size bytes_in_buffer = 0;
        result = input_stream.ReadPartial(input_buffer, sizeof(input_buffer), bytes_in_buffer);
        if (AP4_SUCCEEDED(result)) {
            eos = false;
        } else if (result == AP4_ERROR_EOS) {
            eos = true;
        } else {
            fprintf(stderr, "ERROR: failed to read from input file\n");
            break;
        }
        AP4_Size offset = 0;
        bool     found_access_unit = false;
        do {
            AP4_AvcFrameParser::AccessUnitInfo access_unit_info;
            
            found_access_unit = false;
            AP4_Size bytes_consumed = 0;
            result = parser.Feed(&input_buffer[offset],
                                 bytes_in_buffer,
                                 bytes_consumed,
                                 access_unit_info,
                                 eos);
            if (AP4_FAILED(result)) {
                fprintf(stderr, "ERROR: Feed() failed (%d)\n", result);
                break;
            }
            if (access_unit_info.nal_units.ItemCount()) {
                // we got one access unit
                found_access_unit = true;
                if (Options.verbosity) {
                    printf("H264 Access Unit, %d NAL units, decode_order=%d, display_order=%d\n",
                           access_unit_info.nal_units.ItemCount(),
                           access_unit_info.decode_order,
                           access_unit_info.display_order);
                }
                
                // compute the total size of the sample data
                unsigned int sample_data_size = 0;
                for (unsigned int i=0; i<access_unit_info.nal_units.ItemCount(); i++) {
                    sample_data_size += 4+access_unit_info.nal_units[i]->GetDataSize();
                }
                
                AP4_ByteStream* stream = new AP4_MemoryByteStream();
                // store the sample data
                for (unsigned int i=0; i<access_unit_info.nal_units.ItemCount(); i++) {
                    stream->WriteUI32(access_unit_info.nal_units[i]->GetDataSize());
                    stream->Write(access_unit_info.nal_units[i]->GetData(), access_unit_info.nal_units[i]->GetDataSize());
                }
                
                // add the sample to the track
                //imporatant know sample duation couse video stucks
                sample_table->AddSample(*stream, 0, sample_data_size, 1000, 0, 0, 0, access_unit_info.is_idr);
                stream->Release();
                // free the memory buffers
                access_unit_info.Reset();
            }
            
            offset += bytes_consumed;
            bytes_in_buffer -= bytes_consumed;
        } while (bytes_in_buffer || found_access_unit);
        if (eos) break;
    }

}

static AP4_AvcSequenceParameterSet* CheckSequenceParams(AP4_AvcFrameParser& parser)
{
    // check the video parameters
    AP4_AvcSequenceParameterSet* sps = NULL;
    for (unsigned int i=0; i<=AP4_AVC_SPS_MAX_ID; i++) {
        if (parser.GetSequenceParameterSets()[i]) {
            sps = parser.GetSequenceParameterSets()[i];
            break;
        }
    }
    return sps;
}

static AP4_Array<AP4_DataBuffer> CollectSequenceParams(AP4_AvcFrameParser& parser)
{
    AP4_Array<AP4_DataBuffer> sps_array;
    for (unsigned int i=0; i<=AP4_AVC_SPS_MAX_ID; i++) {
        if (parser.GetSequenceParameterSets()[i]) {
            sps_array.Append(parser.GetSequenceParameterSets()[i]->raw_bytes);
        }
    }
    return sps_array;
}

static AP4_Array<AP4_DataBuffer> CollectPictureParams(AP4_AvcFrameParser& parser)
{
    AP4_Array<AP4_DataBuffer> pps_array;
    for (unsigned int i=0; i<=AP4_AVC_PPS_MAX_ID; i++) {
        if (parser.GetPictureParameterSets()[i]) {
            pps_array.Append(parser.GetPictureParameterSets()[i]->raw_bytes);
        }
    }
    return pps_array;
}

static AP4_AvcSampleDescription* CreateSampleDescription(AP4_AvcFrameParser& parser, AP4_AvcSequenceParameterSet* sps)
{
    unsigned int video_width = 0;
    unsigned int video_height = 0;
    sps->GetInfo(video_width, video_height);
    
    // collect the SPS and PPS into arrays
    AP4_Array<AP4_DataBuffer> sps_array = CollectSequenceParams(parser);
    AP4_Array<AP4_DataBuffer> pps_array = CollectPictureParams(parser);
    
    // setup the video the sample descripton
    return new AP4_AvcSampleDescription(AP4_SAMPLE_FORMAT_AVC1,
                                 video_width,
                                 video_height,
                                 24,
                                 "AVC Coding",
                                 sps->profile_idc,
                                 sps->level_idc,
                                 sps->constraint_set0_flag<<7 |
                                 sps->constraint_set1_flag<<6 |
                                 sps->constraint_set2_flag<<5 |
                                 sps->constraint_set3_flag<<4,
                                 4,
                                 sps_array,
                                 pps_array);
}

static AP4_Track* CreateTrack(AP4_AvcFrameParser& parser, AP4_UI32 track_id)
{
    // check the video parameters
    AP4_AvcSequenceParameterSet* sps = CheckSequenceParams(parser);
    unsigned int video_width = 0;
    unsigned int video_height = 0;
    sps->GetInfo(video_width, video_height);
    
    // setup the video the sample descripton
    AP4_AvcSampleDescription* sample_description = CreateSampleDescription(parser, sps);
    
    // see if the frame rate is specified
    unsigned int video_frame_rate = AP4_MUX_DEFAULT_VIDEO_FRAME_RATE*1000;
    AP4_UI32 movie_timescale      = AP4_FRAGMENTER_OUTPUT_MOVIE_TIMESCALE;
//    AP4_UI32 media_timescale      = video_frame_rate;
    //dont calculate track duration for brawser
    //AP4_UI64 video_track_duration = AP4_ConvertTime(1000*sample_table->GetSampleCount(), media_timescale, movie_timescale);
    //    AP4_UI64 video_media_duration = 1000*sample_table->GetSampleCount();

    // create a sample table (with no samples) to hold the sample description
    AP4_SyntheticSampleTable* empty_sample_table = new AP4_SyntheticSampleTable();
    empty_sample_table->AddSampleDescription(sample_description, false);
    
    // create the track
    return new AP4_Track(AP4_Track::TYPE_VIDEO,
                        empty_sample_table,
                        track_id,                    //  track id
                        movie_timescale,      // movie time scale
                        0, // track duration
                        video_frame_rate,     // media time scale
                        0,                    // media duration
                        "und",                // language
                        video_width<<16,      // width
                        video_height<<16      // height
                        );
}


static AP4_Movie* CreateMovie()
{
    // create the output file object
    AP4_Movie* output_movie = new AP4_Movie(AP4_FRAGMENTER_OUTPUT_MOVIE_TIMESCALE);
    // create an mvex container
        return output_movie;
}

static void AddTracksToMovie(AP4_Movie* movie, AP4_Track* video_track, AP4_Track* audio_track)
{
    AP4_ContainerAtom* mvex = new AP4_ContainerAtom(AP4_ATOM_TYPE_MVEX);
    AP4_MehdAtom*      mehd = new AP4_MehdAtom(0);
    mvex->AddChild(mehd);

    if(audio_track)
    {    AP4_TrexAtom* audio_trex = new AP4_TrexAtom(audio_track->GetId(), 1, 0, 0, 0);
        mvex->AddChild(audio_trex);
    }
    
    if(video_track){
        // add a trex entry to the mvex container
        AP4_TrexAtom* video_trex = new AP4_TrexAtom(video_track->GetId(), 1, 0, 0, 0);
        mvex->AddChild(video_trex);
    }
    

    //    // update the mehd duration
    //    mehd->SetDuration(output_movie->GetDuration());
    // add the mvex container to the moov container
    
    // add the track to the output
    if(audio_track)
        movie->AddTrack(audio_track);
    if(video_track)
        movie->AddTrack(video_track);

    movie->GetMoovAtom()->AddChild(mvex);
}


static FragmentInfo* CreateFragment(AP4_SyntheticSampleTable* sample_table,
                                    AP4_UI32 track_id,
                                    AP4_Track::Type type,
                                    AP4_UI32 sequence_number)
{
    // decide which sample description index to use
    // (this is not very sophisticated, we only look at the sample description
    // index of the first sample in the group, which may not be correct. This
    // should be fixed later)
    unsigned int tfhd_flags =   AP4_TFHD_FLAG_DEFAULT_BASE_IS_MOOF;
    if (type == AP4_Track::TYPE_VIDEO) {
        tfhd_flags |= AP4_TFHD_FLAG_DEFAULT_SAMPLE_FLAGS_PRESENT;
    }
    // setup the moof structure
    AP4_ContainerAtom* moof = new AP4_ContainerAtom(AP4_ATOM_TYPE_MOOF);
    AP4_MfhdAtom* mfhd = new AP4_MfhdAtom(sequence_number);
    moof->AddChild(mfhd);
    AP4_ContainerAtom* traf = new AP4_ContainerAtom(AP4_ATOM_TYPE_TRAF);
    AP4_TfhdAtom* tfhd = new AP4_TfhdAtom(tfhd_flags,
                                          track_id,
                                          0,
                                          1,
                                          0,
                                          0,
                                          0);
    if (tfhd_flags & AP4_TFHD_FLAG_DEFAULT_SAMPLE_FLAGS_PRESENT) {
        tfhd->SetDefaultSampleFlags(0x1010000); // sample_is_non_sync_sample=1, sample_depends_on=1 (not I frame)
    }
    
    traf->AddChild(tfhd);
    
    AP4_TfdtAtom* tfdt = new AP4_TfdtAtom(1,(AP4_UI64)(Options.tfdt_start * AP4_MUX_DEFAULT_VIDEO_FRAME_RATE*1000));
    traf->AddChild(tfdt);
    
    AP4_UI32 trun_flags = AP4_TRUN_FLAG_DATA_OFFSET_PRESENT     |
    AP4_TRUN_FLAG_SAMPLE_DURATION_PRESENT | 
    AP4_TRUN_FLAG_SAMPLE_SIZE_PRESENT;
    AP4_UI32 first_sample_flags = 0;
 
    if (type == AP4_Track::TYPE_VIDEO) {
        trun_flags |= AP4_TRUN_FLAG_FIRST_SAMPLE_FLAGS_PRESENT;
        first_sample_flags = 0x2000000; // sample_depends_on=2 (I frame)
    }
    AP4_TrunAtom* trun = new AP4_TrunAtom(trun_flags, 0, first_sample_flags);
    
    traf->AddChild(trun);
    moof->AddChild(traf);
    
    
    // create a new FragmentInfo object to store the fragment details
    FragmentInfo* fragment = new FragmentInfo(sample_table, 0, moof);
    
    AP4_Ordinal sampleIndex = 0;
    AP4_Sample cur_sample;
    sample_table->GetSample(sampleIndex, cur_sample);
    unsigned int end_sample_index = sample_table->GetSampleCount();
    
    // add samples to the fragment
    AP4_Array<AP4_TrunAtom::Entry> trun_entries;
    fragment->m_MdatSize = AP4_ATOM_HEADER_SIZE;
    for (unsigned int sample_count = 0; sample_count < end_sample_index; ++sample_count) {
        // add one sample
        trun_entries.SetItemCount(sample_count+1);
        AP4_TrunAtom::Entry& trun_entry = trun_entries[sample_count];
        trun_entry.sample_duration                = type == AP4_Track::TYPE_VIDEO && Options.pts
                                                    ? Options.pts / end_sample_index * AP4_MUX_DEFAULT_VIDEO_FRAME_RATE / 1000
                                                    : cur_sample.GetDuration();
        trun_entry.sample_size                    = cur_sample.GetSize();
        trun_entry.sample_composition_time_offset = cur_sample.GetCtsDelta();
        
        fragment->m_MdatSize += trun_entry.sample_size;
        fragment->m_Duration += trun_entry.sample_duration;
        
        // next sample
        sample_table->GetSample(++sampleIndex, cur_sample);
    }
    
    if(type == AP4_Track::TYPE_AUDIO)
        Options.pts = 0.125 * 1024 * end_sample_index * 1000;
    
    fprintf(stderr, "%s Fragment Duration: %d\n", type == AP4_Track::TYPE_VIDEO ? "TYPE_VIDEO": "TYPE_AUDIO", fragment->m_Duration);
    
    // update moof and children
    trun->SetEntries(trun_entries);
    trun->SetDataOffset((AP4_UI32)moof->GetSize()+AP4_ATOM_HEADER_SIZE);

    return fragment;
}

static void WriteInitialSegment(AP4_ByteStream& output_stream, AP4_Movie* movie)
{
    // setup the brands
    AP4_Array<AP4_UI32> brands;
    brands.Append(AP4_FILE_BRAND_ISOM);
    brands.Append(AP4_FILE_BRAND_MP42);
    brands.Append(AP4_FILE_BRAND_AVC1);
    //brands.Append(AP4_FILE_BRAND_ISO5);
    
    // write the ftyp atom
    AP4_FtypAtom* ftyp = new AP4_FtypAtom(AP4_FILE_BRAND_MP42, 1, &brands[0], brands.ItemCount());
    ftyp->Write(output_stream);
    delete ftyp;
    
    // write the moov atom
    movie->GetMoovAtom()->Write(output_stream);
}

static void WriteFragment(AP4_ByteStream& output_stream, FragmentInfo* fragment)
{
    AP4_Result result;
    // write the moof
    fragment->m_Moof->Write(output_stream);
    
    // write mdat
    output_stream.WriteUI32(fragment->m_MdatSize);
    output_stream.WriteUI32(AP4_ATOM_TYPE_MDAT);
    AP4_DataBuffer sample_data;
    AP4_Sample     sample;
    AP4_SyntheticSampleTable* sample_table = fragment->m_sample_table;
    for (unsigned int i=0; i<sample_table->GetSampleCount(); i++) {
        // get the sample
        result = sample_table->GetSample(i, sample);
        // read the sample data
        result = sample.ReadData(sample_data);
        // write the sample data
        result = output_stream.Write(sample_data.GetData(), sample_data.GetDataSize());
    }
}

static void WriteMfra(AP4_ByteStream& output_stream, FragmentInfo* fragment)
{
    AP4_TfraAtom* tfra = new AP4_TfraAtom(0);
    tfra->AddEntry(fragment->m_Timestamp, fragment->m_MoofPosition);
    
    // create an mfra container and write out the index
    AP4_ContainerAtom mfra(AP4_ATOM_TYPE_MFRA);
    mfra.AddChild(tfra);
    
    AP4_MfroAtom* mfro = new AP4_MfroAtom((AP4_UI32)mfra.GetSize()+16);
    mfra.AddChild(mfro);
    mfra.Write(output_stream);
}

static FragmentInfo* ProcessH264Track(AP4_ByteStream& input_stream,
                         AP4_ByteStream& output_stream,
                         AP4_Movie* output_movie,
                         AP4_Track** output_track,
                         bool fragment_only)
{

    // create a sample table
    AP4_SyntheticSampleTable* sample_table = new AP4_SyntheticSampleTable();
    // parse the input
    AP4_AvcFrameParser parser;
    ParseH264Stream(input_stream, parser, sample_table);
    AP4_UI32 track_id = 1;
    *output_track = NULL;
    if(!fragment_only)
    {
        // create the track
        *output_track = CreateTrack(parser, track_id);
        //AddTrackToMovie(output_movie, output_track);
       // WriteInitialSegment(output_stream, output_movie);
    }
    
    // create a new FragmentInfo object to store the fragment details
    FragmentInfo* fragment = CreateFragment(sample_table, track_id, AP4_Track::TYPE_VIDEO, 1);
    return fragment;
    
    // remember the time and position of this fragment
    //output_stream.Tell(fragment->m_MoofPosition);
    
   // WriteFragment(output_stream, fragment);
    
    //WriteMfra(output_stream, fragment);
    
    //delete fragment->m_Moof;
    //delete fragment;
}

static FragmentInfo* ProcessAACTrack(AP4_ByteStream& input_stream,
                        AP4_ByteStream& output_stream,
                        AP4_Movie* output_movie,
                        AP4_Track** output_track,
                        bool fragment_only)
{
    // create a sample table
    AP4_UI32 track_id = 2;
    AP4_SyntheticSampleTable* sample_table = new AP4_SyntheticSampleTable();
    AP4_Track* audioTrack = NULL;
    ParseAacTrack(input_stream, sample_table, &audioTrack, track_id);
    fprintf(stderr, "(%d) audio samples \n", sample_table->GetSampleCount());

    *output_track = NULL;
    if(!fragment_only)
    {
        // create the track
        *output_track = audioTrack;
        //AddTrackToMovie(output_movie, audioTrack);
    }
    // create a new FragmentInfo object to store the fragment details
    FragmentInfo* fragment = CreateFragment(sample_table, track_id, AP4_Track::TYPE_AUDIO, 2);
    return fragment;
}

/*----------------------------------------------------------------------
|   main
+---------------------------------------------------------------------*/
int
main(int argc, char** argv)
{
    if (argc < 2) {
        PrintUsageAndExit();
    }

    // init the variables
    const char*  input_filename                = NULL;
    const char*  output_filename               = NULL;
    AP4_Result   result;

    Options.verbosity             = 3;
    Options.debug                 = false;
    Options.tfdt_start            = 0.0;
    Options.sequence_number_start = 1;
    Options.force_i_frame_sync    = AP4_FRAGMENTER_FORCE_SYNC_MODE_NONE;
    Options.fragment_only = false;
    Options.pts = 0;
    Options.audio_file = NULL;
    
    // parse the command line
    argv++;
    char* arg;
    while ((arg = *argv++)) {
        if (!strcmp(arg, "--fragment_only")) {
            Options.fragment_only = true;
        } else if (!strcmp(arg, "--pts")) {
            arg = *argv++;
            Options.pts = strtoul(arg, NULL, 10);
        } else if (!strcmp(arg, "--audio")) {
            arg = *argv++;
            Options.audio_file = arg;
        } else if (input_filename == NULL) {
            input_filename = arg;
        } else if (output_filename == NULL) {
            output_filename = arg;
        } else {
            fprintf(stderr, "ERROR: unexpected argument '%s'\n", arg);
            return 1;
        }
    }
    
    if (Options.debug && Options.verbosity == 0) {
        Options.verbosity = 1;
    }
    
    AP4_ByteStream* input_stream;
    result = AP4_FileByteStream::Create(input_filename, AP4_FileByteStream::STREAM_MODE_READ, input_stream);
    if (AP4_FAILED(result)) {
        fprintf(stderr, "ERROR: cannot open input file '%s' (%d))\n", input_filename, result);
        return 1;
    }
    
    // create the output file object
    AP4_Movie* output_movie = CreateMovie();
    
    AP4_ByteStream* output_stream = NULL;
    result = AP4_FileByteStream::Create(output_filename,
                                        AP4_FileByteStream::STREAM_MODE_WRITE,
                                        output_stream);
    if (AP4_FAILED(result)) {
        fprintf(stderr, "ERROR: cannot create/open output (%d)\n", result);
        return 1;
    }
    

    
    AP4_ByteStream* input_audio = NULL;
    FragmentInfo* audio_frag = NULL;
    AP4_Track* audio_track = NULL;
    if(Options.audio_file)
    {
        AP4_Result result = AP4_FileByteStream::Create(Options.audio_file, AP4_FileByteStream::STREAM_MODE_READ, input_audio);
        if (AP4_FAILED(result)) {
            fprintf(stderr, "ERROR: cannot open input file '%s' (%d))\n", Options.audio_file, result);
            return 1;
        }
        
        audio_frag = ProcessAACTrack(*input_audio, *output_stream, output_movie, &audio_track, Options.fragment_only);
    }
    
    AP4_Track* video_track = NULL;
    FragmentInfo* video_frag = NULL;
    video_frag = ProcessH264Track(*input_stream, *output_stream, output_movie, &video_track, Options.fragment_only);
    
    if(!Options.fragment_only)
    {
        AddTracksToMovie(output_movie, video_track, audio_track);
        WriteInitialSegment(*output_stream, output_movie);
    }
    
    if(video_frag)
    {
        WriteFragment(*output_stream, video_frag);
        delete video_frag;
    }

    if(audio_frag)
    {
        WriteFragment(*output_stream, audio_frag);
        delete audio_frag;
    }
  
    delete output_movie;
    
    // cleanup and exit
    if (output_stream) output_stream->Release();
    if (input_stream) input_stream->Release();
    if (input_audio) input_audio->Release();
    return 0;
}
