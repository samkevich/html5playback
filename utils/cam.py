import numpy as np
import cv2
import subprocess

FFMPEG = "ffmpeg -y -i {i} -c:v libx264 -f mp4 {o}"
MP4FRAG = "mp4fragment {i} {o}"

def execute(cmd):
    print cmd
    subprocess.check_call(cmd, shell = True)

cap = cv2.VideoCapture(0)
count = 0

while not cap.isOpened():
    print "Error"

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    #with open('frame.jpg', 'wb') as f:
    #    ret, jpeg = cv2.imencode('.jpg', frame)
    #    f.write(jpeg.tobytes())
    #execute(FFMPEG.format(i = "frame.jpg", o = "tmp.mp4"))
    #execute(MP4FRAG.format(i = "tmp.mp4", o = "frame{}.mp4".format(count)))
    #count+=1
    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
