import live555
import threading
import subprocess
import struct


URL = 'rtsp://testuser:synesis2014@192.168.2.139:554/onvif/profile2/media.smp'
MP4MUX =  "mp4mux --verbose --track h264:{i} {o}"
MP4FRAG = "mp4fragment {i} {o}"

def execute(cmd):
    subprocess.check_call(cmd, shell = True)

def oneFrame(codecName, data, sec, usec, durUSec):
    if codecName == 'H264':
        print('frame for %s: %d bytes %d %d %d' % (codecName, len(data), sec, usec, durUSec))
        print struct.unpack('!B', data[0])[0]
        if struct.unpack('!B', data[0])[0] != 103:
            with open('frame.h264', 'ab') as f:
                f.write(b'\0\0\0\1' + data)
        else:
            execute(MP4MUX.format(i = "frame.h264", o = "tmp.mp4"))
            with open('frame.h264', 'wb') as f:
                f.write(b'\0\0\0\1' + data)
            #execute(MP4FRAG.format(i = "tmp.mp4", o = "frame.mp4"))

useTCP = False
live555.startRTSP(URL, oneFrame, useTCP)
# Run Live555's event loop in a background thread:
t = threading.Thread(target=live555.runEventLoop, args=())
t.setDaemon(True)
t.start()

#input("Press any KEY to exit...")
raw_input("Press any KEY to exit...")

live555.stopEventLoop()
t.join()

