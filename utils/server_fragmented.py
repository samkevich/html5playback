#!/usr/bin/python -O

import socket, threading, time, base64, hashlib, struct, binascii
import simplejson as json
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer

from mp4file.mp4file import Mp4File

FILE_NAME = "./fragmented.mp4"
prefetch = 10

class Signaling(WebSocket):

  def init(self):
    if 'inited' in self.__dict__ and self.inited == True:
        return
    mp4file = Mp4File(FILE_NAME)
    self.atoms = []
    self.readed = 0
    self.count = 0
    for atom in mp4file.get_atoms():
      #print "%s/%s %d" % (atom.type, atom.name, atom.size)
      self.atoms.append((atom.type, atom.size))
    self.inited = True

  def handleMessage(self):
    try:
      print self.data
      #rng = str(self.data).split()
      #rng = (int(rng[0]), int(rng[1]))
      #print rng
      #d = self.readSegment(rng[0], rng[1]-rng[0])
      #self.sendMessage(d)
      
      self.init()
      toRead = 0
      atom0 = self.atoms[self.count]
      print atom0[0], atom0[1]
      self.count += 1
      atom1 = self.atoms[self.count]
      print atom1[0], atom1[1]
      self.count += 1
      #if (self.count > 20):
      #  self.count = 2
      #  self.readed = self.atoms[0][1] + self.atoms[1][1]
      toRead = atom0[1] + atom1[1]
      print toRead
      d = self.readSegment(self.readed, toRead)
      self.readed += toRead
      self.sendMessage(d)
    except Exception as n:
      print "Unknow error: "+n    

  def readSegment(self, start, length):
    with open(FILE_NAME, "rb") as f:
        f.seek(start) 
        d = f.read(length)
        return bytearray(d)
         
  def handleConnected(self):
    print self.address, 'connected'
    
  def handleClose(self):
    print self.address, 'closed'

if __name__ == '__main__':
  server = SimpleWebSocketServer('', 8800, Signaling)
  server.serveforever()
  
