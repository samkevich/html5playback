#!/usr/bin/python -O

import socket, threading, time, base64, hashlib, struct, binascii
import simplejson as json
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer

from mp4file.mp4file import Mp4File

FILE_NAME = "frame{num}.mp4"

class Signaling(WebSocket):

  def handleMessage(self):
    try:
      print self.data
        
      if "count" not in self.__dict__:
        self.count = 0
      #if self.count != 0: 
      name = FILE_NAME.format(num = self.count) 
      #else: 
      #  name = "./bunny/bunny_480_700kbit_dash.mp4"
      print name, self.count
      self.count += 1
      if self.count > 36:
        self.count = 0
      d = self.readFile(name)
      self.sendMessage(d)
    
    except Exception as n:
      print "Unknow error: "+n    

  def readFile(self, name):
    with open(name, "rb") as f:
        d = f.read()
        return bytearray(d)
         
  def handleConnected(self):
    print self.address, 'connected'
    
  def handleClose(self):
    print self.address, 'closed'

if __name__ == '__main__':
  server = SimpleWebSocketServer('', 8800, Signaling)
  server.serveforever()
  
