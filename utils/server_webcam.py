#!/usr/bin/python -O

import socket, threading, time, base64, hashlib, struct, binascii
import simplejson as json
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer

from mp4file.mp4file import Mp4File

import cv2
import subprocess

FFMPEG =  "ffmpeg -y -i {i} -c:v libx264 -f mp4 {o}"
MP4FRAG = "mp4fragment {i} {o}"

def execute(cmd):
  subprocess.check_call(cmd, shell = True)

class Signaling(WebSocket):

  def handleMessage(self):
    try:
      while True:
        ret, frame = self.cap.read()
        with open('frame.jpg', 'wb') as f:
          ret, jpeg = cv2.imencode('.jpg', frame)
          f.write(jpeg.tobytes())
        execute(FFMPEG.format(i = "frame.jpg", o = "tmp.mp4"))
        execute(MP4FRAG.format(i = "tmp.mp4", o = "frame.mp4"))  
        self.sendMessage(self.readFile("frame.mp4"))
    
    except Exception as n:
      print "Unknow error: "+n    

  def readFile(self, name):
    with open(name, "rb") as f:
        d = f.read()
        return bytearray(d)
         
  def handleConnected(self):
    self.cap = cv2.VideoCapture(0)
    print self.address, 'connected'
    
  def handleClose(self):
    print self.address, 'closed'

if __name__ == '__main__':
  server = SimpleWebSocketServer('', 8800, Signaling)
  server.serveforever()
  
